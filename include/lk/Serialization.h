// Copyright (C) 2022 Lion Kortlepel
#pragma once

#include "BufferView.h"
#include <array>
#include <bit>
#include <cassert>
#include <cmath>
#include <concepts>
#include <cstdint>
#include <cstring>
#include <iterator>
#include <vector>

namespace lk {

namespace detail {
    // modified version from https://stackoverflow.com/a/4887689, under CC BY-SA 2.5
    template<size_t N>
    void byteswap_array(uint8_t (&bytes)[N]) {
        for (uint8_t *p = bytes, *end = bytes + N - 1; p < end; ++p, --end) {
            uint8_t tmp = *p;
            *p = *end;
            *end = tmp;
        }
    }
}

// modified version from https://stackoverflow.com/a/4887689, under CC BY-SA 2.5
template<std::integral T>
T byteswap(T value) {
    detail::byteswap_array(*reinterpret_cast<uint8_t(*)[sizeof(value)]>(&value));
    return value;
}

class SerializationSizeCalculator {
public:
    SerializationSizeCalculator() = default;

    template<std::integral T>
    SerializationSizeCalculator& add() {
        m_total_bytes += sizeof(T);
        return *this;
    }

    template<std::floating_point T>
    SerializationSizeCalculator& add() {
        // exp, x via frexp()
        m_total_bytes += sizeof(int) + sizeof(T);
        return *this;
    }

    template<std::integral T>
    SerializationSizeCalculator& add(T) {
        m_total_bytes += sizeof(T);
        return *this;
    }

    template<std::floating_point T>
    SerializationSizeCalculator& add(T) {
        m_total_bytes += sizeof(int) + sizeof(T);
        return *this;
    }

    template<std::input_iterator IterT>
    SerializationSizeCalculator& add(const IterT& begin, const IterT& end) {
        m_total_bytes += sizeof(*begin) * (end - begin);
        return *this;
    }

    size_t result() const { return m_total_bytes; }

private:
    size_t m_total_bytes { 0 };
};

template<std::integral T>
inline void serialize(BufferView& view, T n) noexcept {
    assert(view.wp + sizeof(n) <= view.data + view.size);
    [[likely]] if constexpr (std::endian::native != std::endian::big) {
        n = byteswap(n);
    }
    std::memcpy(view.wp, reinterpret_cast<uint8_t(*)[sizeof(n)]>(&n), sizeof(n));
    view.wp += sizeof(n);
}

template<std::floating_point T>
inline void serialize(BufferView& view, T f) {
    int exp {};
    T r = std::frexp(f, &exp);
    assert(view.wp + sizeof(exp) + sizeof(r) <= view.data + view.size);
    serialize(view, exp);
    serialize(view, *reinterpret_cast<uint64_t*>(&r));
}

template<std::input_iterator IterT>
inline void serialize(BufferView& view, const IterT& begin, const IterT& end) {
    for (IterT iter = begin; iter != end; ++iter) {
        serialize(view, *iter);
    }
}

// TODO: Should be std::output_iterator, but requires a type
template<std::input_or_output_iterator IterT>
inline void deserialize(BufferView& view, IterT begin, IterT end) noexcept {
    for (IterT iter = begin; iter != end; ++iter) {
        deserialize(view, *iter);
    }
}

template<std::integral T>
inline void deserialize(BufferView& view, T& n) noexcept {
    std::memcpy(reinterpret_cast<uint8_t(*)[sizeof(n)]>(&n), view.wp, sizeof(n));
    [[likely]] if constexpr (std::endian::native != std::endian::big) {
        n = byteswap(n);
    }
    view.wp += sizeof(n);
}

template<std::floating_point T>
inline void deserialize(BufferView& view, T& f) {
    int exp {};
    T r {};
    deserialize(view, exp);
    deserialize(view, *reinterpret_cast<uint64_t*>(&r));
    f = std::ldexp(r, exp);
}

// Inefficient, but easy-to-use serializer.
// Use only for low-performance applications
class SimpleSerializer {
public:
    template<typename... T>
    SimpleSerializer& serialize(T&&... n) {
        m_calc.add(std::forward<T>(n)...);
        auto oldsize = m_buffer.size();
        m_buffer.resize(m_calc.result());
        BufferView view(m_buffer.data(), m_buffer.size());
        view.wp = view.data + oldsize;
        lk::serialize(view, std::forward<T>(n)...);
        return *this;
    }

    const std::vector<uint8_t>& buffer() const;

private:
    std::vector<uint8_t> m_buffer;
    SerializationSizeCalculator m_calc {};
};

// Inefficient, but easy-to-use deserializer.
// Use only for low-performance applications
class SimpleDeserializer {
public:
    SimpleDeserializer(BufferView& view)
        : m_view(view) { }
    template<typename... T>
    SimpleDeserializer& deserialize(T&&... n) {
        lk::deserialize(m_view, std::forward<T>(n)...);
        return *this;
    }

private:
    BufferView m_view;
};

inline const std::vector<uint8_t>& SimpleSerializer::buffer() const {
    return m_buffer;
}

}
