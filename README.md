# lk::serialize

`lk::serialize` is a low-level, terrifically efficient and simple header-only C++20 serialization library.

It can serialize any primitive types (those which either satisfy the `std::integer` or `std::floating_point` concepts, or iterators to containers which satify `std::input_or_output_iterator`).

`lk::serialize` is schema-less, and not typed (there are no identifiers, type info or headers included with the data).
It's to be used as the low-level engine to a higher-level system, so this is omitted.

## Storage

You may use `serialize()` and `deserialize()`, or only one of them, as long as the other "side" follows the following specifications:

Data is written using a `lk::BufferView`, a "view" into any kind of buffer of contiguous memory. This has an internal `wp`, which is the "write pointer" for new data. 
This means that the contiguous buffer has to have enough space to hold the data. To find out exactly how much it is, a SerializationSizeCalculator can be used, like so:

```cpp
size_t needed_size = lk::SerializationSizeCalculator()
    .add<int>()
    .add<float>()
    .result();
```

In this example, we're asking the calculator how much space we need to store an int and a float. 

Similarly, you can pass instances of objects (like iterables) to find out their size requirements.

```cpp
std::vector<int> vec;
vec.resize(10);
bool a = false;
double b = 0.35;

size_t needed_size = lk::SerializationSizeCalculator()
    .add(vec)
    .add(a)
    .add(b)
    .add<uint32_t>() // adding this to show you can mix them
    .result();
```

### Integers

Integer types (those which satisfy the `std::integer` concept), are serialized as big-endian, with no further modification.
They are deserialized by converting them from big-endian to native endianness.

### Floating point numbers

Floating point numbers (those which satisfy the `std::floating_point` concept), are serialized in two steps:

1. The number is decomposed via `std::frexp()` into a normalized fraction (x, of the same floating point type) and an integral power of two (exp, of type int32_t).

2. The power of two, exp, is stored as an integer in big-endian.

3. The normalized fraction, x, is interpreted as a `uint64_t` and stored in big-endian.

Deserialization is the same process reversed, with `std::ldexp()` inplace of `std::frexp()`. 

This process is designed like this, to accomodate platforms which may not implement floating point numbers in the standard IEEE way.

### Arrays, vectors, other iterables

Any iterables (those who's iterators satisfy `std::input_or_output_iterator`, or similar), are serialized as a series of other primitives.
Thus, serializing an iterable is analogous to iterating over the iterable yourself and calling serialize() on each element.

In this case, if your iterable is of varying size, consider prepending a size header (by serializing the size as `uint64_t`, for example).


